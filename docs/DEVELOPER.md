Developer guide
===============

How to use, hack and contribute back to PoliSim.

## Features

- SPA (single-page-application) with dynamic routing, see [/src/router/index.js](/src/router/index.js)

- pages are rendered by views, see [views dir](/src/views)

- reusable components, see [components dir](/src/components)

- development mode support:

  - components are highlighted with pastel colors and have an hint to their respective source file

  - source maps are installed and allow debugging of components in [Chrome DevTools](https://developers.google.com/web/tools/chrome-devtools/)

  - [Vue.js devtools](https://chrome.google.com/webstore/detail/vuejs-devtools/nhdogjmejiglipccpnnnanhbledajbpd) Chrome extension allows live inspection of routes and models

  - to avoid CORS issues during development the front-end proxies the API back-end under `/api`

To start the services you can either use Docker (preferred) or install locally.

## Docker set-up

### Start in production mode

[Authenticate with the container registry](https://docs.gitlab.com/ee/user/packages/container_registry/#authenticate-with-the-container-registry), then download the pre-built images and start the services with this command:

    docker-compose -f production.yml up

By default the app will be served at: http://localhost:8080/test/ (note the trailing slash).

If you want to reconfigure it to be served from a different relative path, change the value of the environment variable `BASE_URL` in `production.yml` (no slashes allowed).

To refresh the pre-built images:

    docker-compose -f production.yml pull

### Start in local development mode

This command builds the images if required, then starts the services and mounts the container volumes from the source tree so that modifying files immediately has an effect:

    docker-compose -f local.yml up

Once the service is started:

- the User Interface (front-end) can be accessed at http://localhost:8080/ (note the trailing slash).

- the front-end proxies the API back-end under `/api` (to avoid CORS issues during development), for example: http://localhost:8080/api/version

- you can also browse GET API endpoints directly on the back-end, for example the same endpoint can be accessed at: http://localhost:8000/version.

To manualy force rebuild of the images (if dependencies have changed), download https://ssd.mathworks.com/supportfiles/downloads/R2019a/Release/9/deployment_files/installer/complete/glnxa64/MATLAB_Runtime_R2019a_Update_9_glnxa64.zip (~2 GB) in the `.cache` dir inside the repo home (`polisim/.cache` dir) then:

    docker-compose -f local.yml build

**NOTE**: On Windows, launch the `docker-compose` command above from within the Debian bash shell inside in the repo home (`polisim` dir).

## Local set-up

Configure the services to run on the same host:

    VITE_MQTT_HOST=localhost
    export VITE_MQTT_HOST
    VITE_BACKEND_HOST=localhost
    export VITE_BACKEND_HOST

then start them as follows.

### MQTT server

Start:

    sudo systemctl start mosquitto.service

Stop:

    sudo systemctl stop mosquitto.service

View status:

    sudo systemctl status mosquitto.service

Continuously view logs:

    sudo journalctl -f -u mosquitto.service

To reset all retained variables, stop the service, delete the `/var/lib/mosquitto/mosquitto.db` file and restart the service:

    sudo systemctl stop mosquitto.service
    sudo rm /var/lib/mosquitto/mosquitto.db
    sudo systemctl start mosquitto.service
    
### Back-end

Enable the dummy bridge by changing `py/bridge_routes.py`:

```diff
- import prov_lug1 as bridge
+ import prova_dummy as bridge
```

then start the back-end on port 8000 with:

```
py/server.py --debug
```

### Front-end

#### Compiles and hot-reloads for development on port 8080

```
yarnpkg serve
```

#### Compiles and minifies for production

```
yarnpkg build
```

#### Lints and fixes files

```
yarnpkg lint
```

## Reference

This project has been generated with [vue-cli 3](https://cli.vuejs.org) and has these CLI Plugins installed:

- [babel](https://github.com/vuejs/vue-cli/tree/dev/packages/%40vue/cli-plugin-babel)
- [router](https://github.com/vuejs/vue-cli/tree/dev/packages/%40vue/cli-plugin-router)
- [eslint](https://github.com/vuejs/vue-cli/tree/dev/packages/%40vue/cli-plugin-eslint)

To customize the vue-cli configuration: see [vue-cli Configuration Reference](https://cli.vuejs.org/config/).

**Essential Links**

- [Core Docs](https://vuejs.org)
- [Forum](https://forum.vuejs.org)
- [Community Chat](https://chat.vuejs.org)

**Ecosystem**:

- [vue-router](https://router.vuejs.org)
- [vuex](https://vuex.vuejs.org)
- [vue-devtools](https://github.com/vuejs/vue-devtools#vue-devtools)
- [vue-loader](https://vue-loader.vuejs.org)
- [awesome-vue](https://github.com/vuejs/awesome-vue)
