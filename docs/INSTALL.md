# Installation guide

How to install the PoliSim demos.

## Preparation

Clone this repo:

    git clone git@gitlab.com:simevo/polisim.git

**NOTE** on Windows, it is preferred to use `git` from WSL 2:

- follow the [Windows Subsystem for Linux 2 Installation Guide](https://docs.microsoft.com/en-us/windows/wsl/install-win10)

- install the [Debian "app" from Microsoft Store](https://www.microsoft.com/en-us/p/debian/9msvkqc78pk6) 

- open the Debian bash shell and issue these commands to install `git`:

    ```sh
    sudo apt update
    sudo apt upgrade
    sudo apt install git
    ```

    (on Windows, when prompted for the `sudo` password, enter the password you created for the Debian WSL virtual machine)

- now clone the repo like this:

    ```sh
    git clone git@gitlab.com:simevo/polisim.git
    ```

**NOTE** to grant access to the `polisim` private repo, you have to [configure your ssh keys in gitlab](https://gitlab.com/-/profile/keys)

The files will be downloaded to `\\wsl$\Debian\home\<username>\polisim`.

For more information:

- https://docs.docker.com/desktop/windows/wsl/#best-practices
- https://wiki.debian.org/InstallingDebianOn/Microsoft/Windows/SubsystemForLinux
- https://docs.microsoft.com/en-us/windows/wsl/interop
- https://devblogs.microsoft.com/commandline/access-linux-filesystems-in-windows-and-wsl-2/
- https://devblogs.microsoft.com/commandline/integrate-linux-commands-into-windows-with-powershell-and-the-windows-subsystem-for-linux/
- https://devblogs.microsoft.com/commandline/per-directory-case-sensitivity-and-wsl/
- https://devblogs.microsoft.com/commandline/cross-post-wsl-interoperability-with-docker/

## Docker set-up

The docker set-up is preferred.

### Requirements

macOS 10.13 (High Sierra) or later: install Docker Desktop for Mac from https://docker.com (make sure Docker Desktop is running !)

**NOTE**: The backend can only run on containers with the `amd64` architecture, this means the dockerized app won't run on macs with Apple Silicon chips.

Debian 11 (bullseye):
```
sudo apt install docker.io
```

Fedora 32/33: https://docs.docker.com/engine/install/fedora/

Windows: install Docker Desktop for Windows (with Linux containers) from: https://hub.docker.com/editions/community/docker-ce-desktop-windows/; then enable WSL 2 integration: https://docs.docker.com/docker-for-windows/wsl/.

## Kubernetes deploy

Install [minikube](https://minikube.sigs.k8s.io/docs/start/) and [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/#install-using-native-package-management).

Configure the cluster to pull images from the private registry on gitlab.simevo.com (https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/):

    kubectl create secret generic regcred --from-file=.dockerconfigjson=docker_config.json --type=kubernetes.io/dockerconfigjson

Download this repo:

    git clone git@gitlab.simevo.com:simevo/polisim.git
    cd polisim

Deploy to minikube:

    kubectl apply -f deployment.yaml

[Export the service from minikube](https://minikube.sigs.k8s.io/docs/commands/service/):

    minikube service --url polisim

this will return something like `http://192.168.39.204:32080`. You can directly access that url on the host where minikube is running; if you are on a different host you must forward the port with:

    ssh -L 8080:192.168.39.204:31360 example.com
    
then you will be able to access the service at http://localhost:8080.

To deploy a separate instance on anoher pod, duplicate `deployment.yaml`, rename service + deployment and deploy the new application:

    cp deployment.yaml deployment1.yaml
    sed -i 's/name: polisim/name: polisim1/g' deployment1.yaml

When you're done, tear down the deployments:

    kubectl delete -f deployment.yaml
    kubectl delete -f deployment1.yaml

## Local install

### Prerequisites on Debian 11 (bullseye):

```
sudo apt install python3-cherrypy3 python3-bottle mosquitto python3-paho-mqtt yarnpkg
```

### Prerequisites on MacOS:

```
brew update
brew upgrade python 
brew install yarn
pip3 install bottle cherrypy3
```

### Install and configure

Configure moquitto to use the websockets protocol creating a file `/etc/mosquitto/conf.d/websockets.conf` with this content:

    protocol websockets

and restarting the service:

    sudo systemctl restart mosquitto.service
    sudo systemctl status mosquitto.service

Install node dependencies with:

```
yarnpkg
```
