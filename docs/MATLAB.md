Model development with MATLAB®
==============================

How to export an existing MATLAB® dynamic process model for integration with PoliSim

MATLAB® is a registered trademark of [The MathWorks, Inc.](https://www.mathworks.com/).

## Concepts

The PoliSim Python backend must serve the requests coming via RESTful API from the HMI, such as:

- reset the simulation

- set one or more variables to some values

- run the simulation for 500 ms of simulation time (for the simulation to be realistic, the model should respond in less than 400 ms) and return the final simulation time, and an array of values for an hardwired list of results we are interested in

- download the time profiles for all results we are interested in

To integrate a MATLAB® dynamic process model with PoliSim, the model must be prepared and exported as **bridge** Python library using the [MATLAB® Engine API for Python](https://www.mathworks.com/help/matlab/matlab_external/install-the-matlab-engine-for-python.html).

The **bridge** must:

1. be _stateful_, i.e. during a session, the time and status of the simulation must be retained between successive calls to the bridge

2. be totally _headless_ i.e. it should not open any window, plots, graph etc.

## Model preparation

Define two fixed arrays of one or more variables:

- `inputs`

- `results`

**NOTE**: in una prima versione potremmo accontentarci di provare a passare solo valori scalari (un input e un risultato)

Define 3 functions:

- `restart()` sets the simulation time to 0 and brings all variables back to their initial values (a safe state)

- `result_values = STEP(input_values)` moves forward the simulation of the specified `deltat` time interval, then returns the simulation time `t` and the `results`

  - `input_values` (input) array of floating points, the first element is deltat (the time interval in seconds), next come the input values as per `inputs` array

  - `result_values` (output) array of floating points, the first element is t (the new simulation time in seconds), next come the result values as per `results` array
 
  **NOTE**: this function is synchronous (i.e. the function returns when the simulation step is complete)

- `dump(filename)` exports a MATLAB® `.m` file that contains the time profiles for all results (can be used for plotting)

  - `filename` name of the desired MATLAB® `.m` file, string

## Model export

Export the model as a Python "bridge" library using the [MATLAB® app "Library Compiler"](https://www.mathworks.com/help/compiler_sdk/gs/create-a-python-application-with-matlab-code.html):

1. open the Library Compiler app from the MATLAB® command prompt by entering `libraryCompiler`

2. In the Type section of the toolstrip, click Python Package.

3. In the _Library Compiler app_ project window, specify the files of the MATLAB® application that you want to deploy. Repeat these steps to package multiple functions in the same application:

  - In the _Exported Functions_ section of the toolstrip, click +

  - In the _Add Files_ window, browse to the example folder, and select the function you want to package. Click Open.

4. In the _Packaging Options_ section of the toolstrip, select _Runtime downloaded from web_

5. Specify Package Settings:

  - set the _Library Name_ to **bridge**
  
  - set a unique library version
  
  - in the _Samples_ section, generate the Sample Driver files (at least one).

All this produces a `bridge.zip` file containing the `for_testing` directory with `setup.py` and a `bridge` subdirectory which contains the Component Technology File (CFT) where the model details are stored in a proprietary format.

## Model installation on Windows

Tested on Windows 10 64 bit using Python 3 64-bit (3.7.8) installed with Microsoft Visual Studio Community 2019.

Extract `setup.py` and the `bridge` subdirectory from the `bridge.zip` exported with MATLAB® into `matlab` dir.

Install the MATLAB® Runtime version R2019a (9.6) 64-bit for Windows from https://www.mathworks.com/products/compiler/mcr/ (download size: 1.8 GB, unzipped size: 1,9 GB, installed size: 4 GB).

Open a Command window, make sure Python 3 is in the path:

    python --version

If it is not but is already installed, add the path where `python.exe` is located (for example: `C:\Program Files (x86)\Microsoft Visual Studio\Shared\Python37_64`) to your profile's environment variables and restart the Command window.

Else just type `python` and install the app from the Windows Store (for details see: https://devblogs.microsoft.com/python/python-in-the-windows-10-may-2019-update/).

Change to the directory `matlab` directory and execute the command:

    python setup.py install --user --record files.txt
    
to install the Python bridge module to your home folder. For details, see: [Installing Python Modules (Python 3)](https://docs.python.org/3.7/installing/index.html).

To uninstall, delete all files listed in `files.txt`; see: [this SO answer](https://stackoverflow.com/a/1550235) (normally they are in `C:\Users\<username>\AppData\Roaming\Python\Python37\site-packages`)

## Model use

Import the `bridge` package:

    import bridge

Initialize the package consisting on one or more deployed MATLAB® functions:

    handle = bridge.initialize()

Inspect available variables and functions with:

    dir(handle)

Launch the functions provided by the module:

    handle.restart(0)
    input_values = [0.7] + [0.0] * 32
    result_values = handle.STEP(input_values)
    print(result_values)

Close the package:

    handle.terminate()
