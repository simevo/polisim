PoliSim
=======

PoliSim is a template architecture that can be used to interface simulation models with the real-world and with HMI (Human-Machine Interfaces).

Key value points:

- modular

- open source and mostly based on 3-rd party open-source building blocks

- does not rely on process control-specific, non-open standards

- does not require the license of a commercial SCADA

## Architecture

1. **MQTT broker** (i.e. [mosquitto](https://mosquitto.org)) for data exchange

1. (optional) SQL database (i.e. [PostgreSQL](http://postgresql.org/)) for persistence of historical data; samples data from the MQTT broker and stores them for historical data visualization

1. Simulation **engine**; different simulation technologies are supported

1. **Back-end**: Python script that controls the execution of the simulation engine, updating the inputs and the outputs at a fixed communication interval (i.e. 200 ms); interfaces to the MQTT broker using the Python library [paho-mqtt](https://www.eclipse.org/paho/clients/python/) and to the database using the [Psycopg library](https://www.psycopg.org/)

1. **Front-end**: Supervisory Control and Data Acquisition (SCADA) dashboard and HMI, to visualize and control the status of the components; implemented as a webapp SPA (Single Page Application) in HTML5+CSS+JavaScript with the [MVVM JavaScript framework Vue.js](https://vuejs.org/), that communicates with the back-end via RESTful API and with the MQTT broker as in [1](https://medium.com/@jamomani/mqtt-over-websocket-in-a-react-app-35ce96cd0844) or [2](https://github.com/nik-zp/vue-mqtt)

## Documentation

- [Installation guide](docs/INSTALL.md): how to install the demos

- [Development guide](docs/DEVELOPER.md): how to use the demos, hack and contribute back to this project

- [Model development with MATLAB® ](docs/MATLAB.md): how to export an existing MATLAB® dynamic process model for integration with PoliSim

## Applications

The template architecture can be spelled out for different applications:

- Digital twins

- OTS (Operator Training System)

- Soft Sensors

- Parallel Simulation applications

### Operator Training System (OTS)

This application will be developed first, with the focus on the real-time simulation of emergency scenarios for educational purposes of an intensified process for treatment of VOC-containing fumes.

The simulation engine will be provided by MATLAB®, re-using an existing MATLAB® model for the VOC RFR reactor.

The Python scripts will connect to the Engine via the [MATLAB® Engine API for Python](https://www.mathworks.com/help/matlab/matlab_external/install-the-matlab-engine-for-python.html).

![architecture for OTS configuration](images/ots.png "Architecture for OTS configuration")

Initially the application will be limited:

- one-page process scheme

- no historian capabilties

- read: max 10 analog tags + 10 digital tags

- write: max 10 analog tags + 10 digital tags

### Soft Sensor

In this configuration, the MQTT broker acts as a gateway to the process data in the DCS.

The front-end is provided by the DCS client SCADA HMI.

![architecture for soft sensor configuration](images/soft_sensor.png "Architecture for soft sensor configuration")

### Parallel Simulation

In this configuration, a ling list of variables for the whole plant is managed by the MQTT broker.

Each section of the plant is simulated using a different engine. The heterogeneous simulation engines exchange data via the MQTT broker.

![architecture for parallel simulation configuration](images/parallel.png "Architecture for parallel simulation configuration")

## Authors

- model and UI design: [**DISAT** (Department of Applied Science and Technology)](http://www.disat.polito.it/) @ [Politecnico di Torino](https://www.polito.it)

- software development: [simevo s.r.l.](https://simevo.com/)

- MATLAB® is a registered trademark of [The MathWorks, Inc.](https://www.mathworks.com/)

## License 

European Union Public License 1.2 (EUPL-1.2)
