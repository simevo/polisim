#!/usr/bin/env python3
# coding=utf-8
#
# This file is part of PoliSim
# European Union Public License 1.2 (EUPL-1.2)
# Copyright (C) 2019-2024 simevo s.r.l.


class MyClass:
    time = 0

    def restart(self, t):
        print("restart")
        self.time = t
        return t

    def STEP(self, f, stdin=None, stdout=None, stderr=None):
        print("STEP - inputs = ", f)
        self.time += f[0]
        results = [self.time] + f[1:19]
        print("results = ", results)
        return results

    def terminate(self):
        pass


dummy = MyClass()


def initialize():
    return dummy
