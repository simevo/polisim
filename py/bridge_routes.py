#!/usr/bin/env python3
# coding=utf-8
#
# This file is part of PoliSim
# European Union Public License 1.2 (EUPL-1.2)
# Copyright (C) 2019-2024 simevo s.r.l.

import os

import bottle
import common
import feb22 as bridge
import mqtt

# for debug:
# import prova_dummy as bridge


handle = None


def init():
    global handle
    handle = bridge.initialize()


@bottle.route("/version", method="GET")
def version():
    """returns the bridge API version

    output: the version string in major.minor.patch format, example: `0.0.1`

    sample call:
    curl -i -X GET -H 'Content-Type: application/json'
      -d '{"username":"Aaa", "email": "a.b@lll.com"}' http://localhost:8000/version
    """
    return "0.0.2"


@bottle.route("/restart", method="POST")
def restart():
    """sets the simulation time to 0 and brings all variables back to their initial values (a safe state)

    output: the simulation time (0.0)

    TODO: update state of variables in mqtt !

    sample call:
    curl -i -X POST http://localhost:8000/restart
    """
    global handle
    handle.restart(0.0)
    data = {"time": 0.0}
    return common.render_json(data, 201)


@bottle.route("/step", method="PUT")
def step():
    """run the simulation for a specified time

    input: JSON with simulation time delta (`deltat`)
    output: JSON with the time delta and the final simulation time

    sample call:
    curl -i -X PUT -H 'Content-Type: application/json' -d '{ "deltat": 0.8 }' http://localhost:8000/step
    """
    global handle
    data = bottle.request.json
    if data is None:
        raise bottle.HTTPError(status=400, body="no JSON data received")
    deltat = data["deltat"]
    input_values = list(map(lambda v: float(v), mqtt.inputs()))
    inputs = [float(deltat)] + input_values
    print("inputs = ", inputs)
    results = handle.STEP(inputs)
    data["time"] = results[0]
    print("results = ", results)
    mqtt.publish_results(results[1:])
    return common.render_json(data, 201)


@bottle.route("/environment", method="GET")
def environment():
    """returns the MQTT-over-websockets endpoint base url for the frontend, without trailing slash

    output examples:
    - `wss://saas.example.com/b8b3a0af-9a6b-4511-a789-4721ef0a1971` for production
    - `ws://localhost:8080` for local development

    sample call:
    curl -i -X GET http://localhost:8000/environment
    """
    hostname = os.environ["HOSTNAME_AND_PORT"]
    baseurl = os.environ["BASE_URL"]
    wsprotocol = os.environ["WEBSOCKETS_PROTOCOL"]
    if baseurl == "":
        return wsprotocol + "://" + hostname
    else:
        return wsprotocol + "://" + hostname + "/" + baseurl
