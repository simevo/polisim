#!/usr/bin/env python3
# coding=utf-8
#
# This file is part of PoliSim
# European Union Public License 1.2 (EUPL-1.2)
# Copyright (C) 2019-2024 simevo s.r.l.

import argparse

import bottle
import bridge_routes
import mqtt

# dummy_routes.init()  # avoid flake8 F401
bridge_routes.init()  # avoid flake8 F401


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-d", "--debug", help="enable debugging and autoreloading", action="store_true"
    )
    args = parser.parse_args()
    mqtt.start()

    if args.debug:
        bottle.debug(True)
        bottle.run(host="0.0.0.0", port=8000, reloader=True, server="cherrypy")
    else:
        bottle.run(host="127.0.0.1", port=8000, reloader=False, server="cherrypy")

    mqtt.stop()
