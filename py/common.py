#!/usr/bin/env python3
# coding=utf-8
#
# This file is part of PoliSim
# European Union Public License 1.2 (EUPL-1.2)
# Copyright (C) 2019-2024 simevo s.r.l.

import json

import bottle


def render_json(d, code):
    """sends the supplied JSON object as response
    if the request header accepts application/json, the JSON is dumped in compact form,
    else it is prettyprinted and wrapped in a <pre> HTML element
    """
    accept = bottle.request.headers.get("accept")
    bottle.response.status = code
    if accept.find("application/json") >= 0:
        bottle.response.set_header("content-type", "application/json")
        return json.dumps(d)
    else:
        return "<pre>" + json.dumps(d, indent=2) + "</pre>\n"
