#!/usr/bin/env python3
# coding=utf-8
#
# This file is part of PoliSim
# European Union Public License 1.2 (EUPL-1.2)
# Copyright (C) 2019-2024 simevo s.r.l.

import bottle
import common


def init():
    pass


@bottle.route("/variables", method="GET")
def variables():
    rows = [
        {"id": 1, "tag": "P101", "value": "101325.0", "units": "Pa"},
        {"id": 2, "tag": "P102", "value": "201325.0", "units": "Pa"},
        {"id": 3, "tag": "P103", "value": "401325.0", "units": "Pa"},
        {"id": 4, "tag": "T101", "value": "298.15", "units": "K"},
        {"id": 5, "tag": "T102", "value": "373.15", "units": "K"},
        {"id": 6, "tag": "T103", "value": "273.15", "units": "K"},
        {"id": 7, "tag": "T104", "value": "313.15", "units": "K"},
        {"id": 8, "tag": "T105", "value": "293.15", "units": "K"},
        {"id": 9, "tag": "F101", "value": "0.01", "units": "kg/s"},
    ]
    return common.render_json(rows, 200)


@bottle.route("/people", method="GET")
def people():
    rows = [
        {"id": 1, "first": "Mark", "last": "Otto", "handle": "@mdo"},
        {"id": 2, "first": "Jacob", "last": "Thornton", "handle": "@fat"},
        {"id": 3, "first": "Larry", "last": "the Bird", "handle": "@twitter"},
    ]
    return common.render_json(rows, 200)


@bottle.route("/animals", method="GET")
def animals():
    rows = [
        {
            "id": 1,
            "name": "African Bush Elephant",
            "legs": 4,
            "binomial name": "Loxodonta africana",
        },
        {
            "id": 2,
            "name": "Asian Elephant",
            "legs": 4,
            "binomial name": "Elephas maximus",
        },
        {
            "id": 3,
            "name": "Woolly mammoth",
            "legs": 4,
            "binomial name": "Mammuthus primigenius",
        },
    ]
    return common.render_json(rows, 200)
