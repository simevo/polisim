#!/usr/bin/env python3
# coding=utf-8
#
# This file is part of PoliSim
# European Union Public License 1.2 (EUPL-1.2)
# Copyright (C) 2019-2024 simevo s.r.l.

from os import getenv

import paho.mqtt.client as mqtt

prefix = "polisim/demo1"
client = None
input_tags = [
    "AAH01",
    "TAH03",
    "PDAH03",
    "PAH22",
    "AICA18",
    "TAH11",
    "TAH21",
    "TAH10",
    "TSH21",
    "TSH10",
    "TAHH11",
    "TAHH10",
    "TSHH11",
    "TSHH10",
    "TAL10",
    "TSL10",
    "TALL10",
    "AIA19",
    "AAH01_set",
    "TAH03_set",
    "PDAH03_set",
    "PAH22_set",
    "AICA18_set",
    "TAH11_set",
    "TAH21_set",
    "TAH10_set",
    "TSH21_set",
    "TSH10_set",
    "TAHH11_set",
    "TAHH10_set",
    "TSHH11_set",
    "TSHH10_set",
    "TAL10_set",
    "TSL10_set",
    "TALL10_set",
    "AIA19_set",
    "AT01",
    "FIC05",
    "TI03",
    "PI22",
    "F01",
    "FCV05",
    "FIC05_set",
    "FIC18",
    "FV18",
    "O2",
    "E02",
    "TINV",
    "TAU",
    "I04",
    "XV12",
    "XV13",
    "XV14",
    "XV15",
]
input_values = [
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0.5,
    268,
    11.5,
    1357,
    0.01,
    500,
    500,
    500,
    500,
    500,
    530,
    530,
    530,
    530,
    410,
    410,
    390,
    0.02,
    0.2,
    117.5,
    230,
    20,
    0,
    -1,
    5000,
    0.005,
    -1,
    30,
    -1,
    0,
    120,
    0,
    0,
    0,
    0,
    0,
]
result_tags = [
    "AAH01_",
    "TAH03_",
    "PDAH03_",
    "PAH22_",
    "AICA18_",
    "TAH11_",
    "TAH21_",
    "TAH10_",
    "TSH21_",
    "TSH10_",
    "TAHH11_",
    "TAHH10_",
    "TSHH11_",
    "TSHH10_",
    "TAL10_",
    "TSL10_",
    "TALL10_",
    "AIA19_",
    "AE18_",
    "AE19_",
    "TE17_",
    "T1_",
    "T2_",
    "T3_",
    "T4_",
    "T5_",
    "T6_",
    "T7_",
    "T8_",
    "FIC18_",
    "E02_",
    "TINV_",
    "FIC18_",
    "XV12_",
    "XV13_",
    "XV14_",
    "XV15_",
]
result_values = [0.0] * len(result_tags)


def on_connect(client, userdata, flags, rc):
    topic = prefix + "/#"
    print("mqtt connected, subscribing ", topic)
    client.subscribe(topic)


def on_message(client, userdata, message):
    s = len(prefix) + 1
    tag = message.topic[s:]
    value = message.payload.decode()
    # print(f"Received value `{value}` for tag: `{tag}`")
    if tag in input_tags:
        index = input_tags.index(tag)
        input_values[index] = value


def start():
    """connect in a non-blocking manner with the mqtt server
    starts the mqtt network loop in a separate thread and populates input variables"""
    global client
    if client is None:
        print("mqtt start")
        mqtt_host = getenv("VITE_MQTT_HOST", default="mqtt")
        client = mqtt.Client(transport="websockets")
        client.on_connect = on_connect
        client.on_message = on_message
        client.connect(mqtt_host, port=1883, keepalive=60)
        client.loop_start()
        publish_inputs()
        publish_results(result_values)


def stop():
    """stops the mqtt network loop"""
    global client
    if client is not None:
        print("mqtt stop")
        client.loop_stop(force=False)


def publish(tags, values):
    i = 0
    for tag in tags:
        client.publish(topic=prefix + "/" + tag, payload=values[i], qos=0, retain=True)
        i += 1


def publish_inputs():
    """publish the input values to the mqtt server"""
    print("mqtt publish inputs")
    publish(input_tags, input_values)


def publish_results(values):
    """publish the result values to the mqtt server"""
    print("mqtt publish results")
    publish(result_tags, values)


def inputs():
    """returns the input values as they were last updated by the mqtt server"""
    return input_values
