// This file is part of PoliSim
// European Union Public License 1.2 (EUPL-1.2)
// Copyright (C) 2019-2024 simevo s.r.l.

import {
  createWebHistory,
  createRouter
} from 'vue-router'

const mainRoutes = [
  {
    path: '/',
    name: 'home',
    component: () => import('../views/Home.vue')
  },
  {
    path: '/about',
    name: 'about',
    component: () => import('../views/About.vue')
  }
]

export const routes = mainRoutes

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes
})

export default router
