// This file is part of PoliSim
// European Union Public License 1.2 (EUPL-1.2)
// Copyright (C) 2019-2024 simevo s.r.l.

import { createApp } from 'vue'
import App from './App.vue'
import router from './router'

import 'bootstrap/dist/css/bootstrap.css'

import 'bootstrap/dist/js/bootstrap.min.js'

const app = createApp(App)
app.use(router)
app.mount('#app')
