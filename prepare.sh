#!/bin/sh
# prepares the container for execution

set -e

# if the environment variable BASE_URL (no slashes!) is set, reconfigures the web-app to be served from the path "/$BASE_URL"
if test -z "$BASE_URL"; then
  true
else
  if [ -d /usr/share/nginx/html/eda2a2e6-8f12-4db3-b62b-9899cf98c640 ]; then
    # inject the BASE_URL variable
    sed -i "s/eda2a2e6-8f12-4db3-b62b-9899cf98c640/$BASE_URL/g" /usr/share/nginx/html/eda2a2e6-8f12-4db3-b62b-9899cf98c640/assets/* /usr/share/nginx/html/eda2a2e6-8f12-4db3-b62b-9899cf98c640/index.html
    # moving from eda2a2e6-8f12-4db3-b62b-9899cf98c640 to $BASE_URL
    mv /usr/share/nginx/html/eda2a2e6-8f12-4db3-b62b-9899cf98c640 "/usr/share/nginx/html/$BASE_URL"
  fi
fi
