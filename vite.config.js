import { fileURLToPath, URL } from 'node:url'
import { defineConfig, loadEnv } from 'vite'
import vue from '@vitejs/plugin-vue'
import process from 'node:process'

// https://vitejs.dev/config/
export default defineConfig(({ mode }) => {
  const env = loadEnv(mode, process.cwd(), '')
  const mqttHost = env.VITE_MQTT_HOST || 'mqtt'
  const backendHost = env.VITE_BACKEND_HOST || 'backend'
  return {
    base: mode === 'production' ? '/eda2a2e6-8f12-4db3-b62b-9899cf98c640' : '/',
    server: {
      proxy: {
        '/api': {
          target: `http://${backendHost}:8000`,
          changeOrigin: true,
          rewrite: (path) => path.replace(/^\/api/, '')
        },
        '/mqtt': {
          target: `ws://${mqttHost}:1883`,
          ws: true,
          changeOrigin: true,
          rewrite: (path) => path.replace(/^\/mqtt/, '')
        }
      }
    },
    resolve: {
      alias: {
        '@': fileURLToPath(new URL('./src', import.meta.url))
      }
    },
    plugins: [vue()],
    configureWebpack: {
      devtool: 'source-map'
    },
    lintOnSave: false
  }
})
